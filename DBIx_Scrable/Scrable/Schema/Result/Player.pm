use utf8;
package Scrable::Schema::Result::Player;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Scrable::Schema::Result::Player

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<players>

=cut

__PACKAGE__->table("players");

=head1 ACCESSORS

=head2 player_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 25

=head2 surname

  data_type: 'varchar'
  is_nullable: 0
  size: 25

=head2 date_joined

  data_type: 'date'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 phone

  data_type: 'integer'
  is_nullable: 0

=head2 email

  data_type: 'varchar'
  is_nullable: 0
  size: 25

=cut

__PACKAGE__->add_columns(
  "player_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 25 },
  "surname",
  { data_type => "varchar", is_nullable => 0, size => 25 },
  "date_joined",
  { data_type => "date", datetime_undef_if_invalid => 1, is_nullable => 0 },
  "phone",
  { data_type => "integer", is_nullable => 0 },
  "email",
  { data_type => "varchar", is_nullable => 0, size => 25 },
);

=head1 PRIMARY KEY

=over 4

=item * L</player_id>

=back

=cut

__PACKAGE__->set_primary_key("player_id");

=head1 RELATIONS

=head2 games_looser

Type: has_many

Related object: L<Scrable::Schema::Result::Game>

=cut

__PACKAGE__->has_many(
  "games_looser",
  "Scrable::Schema::Result::Game",
  { "foreign.looser_id" => "self.player_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 games_winners

Type: has_many

Related object: L<Scrable::Schema::Result::Game>

=cut

__PACKAGE__->has_many(
  "games_winners",
  "Scrable::Schema::Result::Game",
  { "foreign.winner_id" => "self.player_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-01-21 15:16:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:idf61X3eApQM85A6GXBOug


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
