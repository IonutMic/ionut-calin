package MyWeb::App;
use Dancer ':syntax';
use DBIx::Class;
use Auth::GoogleAuthenticator;
use Domain::Schema;
use DBI;
use Template;
use strict ;
use warnings ;
use Data::Dumper;
use DBIx::Insert::Multi;
use Dancer2::Plugin::Database;
use POSIX qw(strftime);
use DateTime;
use Template::Plugin::DBI ;
use Digest::HMAC_SHA1 qw/ hmac_sha1_hex /;

my $schema = Domain::Schema -> connect ('dbi:mysql:dbname=Domain_seller_miron;host=172.16.2.17','perl','training');
my $dbh = DBI->connect('dbi:mysql:dbname=Domain_seller_miron;host=172.16.2.17','perl','training'); 

 set 'database'     => File::Spec->catfile(File::Spec->tmpdir(), 'dancr.db');
 set 'session'      => 'Simple';
 set 'template'     => 'template_toolkit';
 set 'logger'       => 'console';
 set 'log'          => 'debug';
 set 'show_errors'  => 1;
 set 'startup_info' => 1;
 set 'warnings'     => 1;

our $VERSION = '0.1';
my ($err,$errregister,$erraccount,$erruser);
my ($crtdomain,$crtuser,$crtaccount);
my $client_id;
my $term;
my $TIME_STEP = 30 ;
srand(time() ^ $$);


get '/login' => sub {
    
    template 'login' , {
        'err'        => $err,
        'login_url'  => '/login',
        'create_user'=>'/create',
    };
};

post '/create' => sub {
    $err = undef;
    return redirect '/create_user';
};

get '/create_user' => sub {
    
    template 'createuser.tt' , {
        'err'        => $err,
        'createuser' => '/create_user',
        'back'       => '/clearaccount',
    };
};

get '/clearuser' => sub {
    $err = undef;
    return redirect '/create_user/account';
};

post '/create_user' => sub {
    my $params = request->params;
    if (!$params->{'Name'}||!$params->{'Surname'}||!$params->{'Adress'}||!$params->{'Country'}||!$params->{'City'}){
        $err = 'Please complete the mandatory fields marked with * and add a phone or email';
        return redirect '/create_user';
    }
        if ((!$params->{'Email'} || !($params->{'Email'} =~ m/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i))  && !$params->{'Phone'}){
            $err = 'Please add a phone number or email adress';
            return redirect '/create_user';
        }
        else{
            my $newuser = $schema->resultset('Client')->new(
                {
                    client_name    => $params->{'Name'},
                    client_surname => $params->{'Surname'},
                    client_adress  => $params->{'Adress'},
                    client_city    => $params->{'City'},
                    client_country => $params->{'Country'},
                    client_phone   => $params->{'Phone'},
                    client_email   => $params->{'Email'},
                    admin          => '0',
                }
            );
            $newuser -> insert();
            $client_id = $newuser->get_column('client_id');
            print Dumper($client_id);
            return redirect '/clearuser';
        }
};

get '/create_user/account' => sub {
    template 'createaccount.tt' , {
        'err' => $err,
        'createaccount' => '/create_user/account',
    }
};

get '/clearaccount' => sub {
    $err = undef;
    return redirect '/login' ;
};

post '/create_user/account' => sub {
    my $params = request->params;
    if (!$params->{'Password'} || !$params->{'Account'}){
        $err = 'Please insert a valid account and password';
        return redirect '/create_user/account';
    }
    else {
        my $newaccount = $schema->resultset('Account')->new(
            {
                account_user     => $params->{'Account'},
                account_password => $params->{'Password'},
                account_owner    => $client_id,
                account_key      =>  generateBase32Secret()
            }
        );
        $newaccount -> insert();
        return redirect '/clearaccount';
    }
};

post '/login' => sub {
    my $params= request->params;
    my $username;
    my $account_user = $schema->resultset('Account')->find(
        {
            account_user => $params->{'username'},
        }
    );
    my $account_password = $schema->resultset('Account')->find(
        {
            account_password => $params->{'password'}
        }
    );
    if (!$account_user ||  !$account_password ){
        $err = 'Invalid username or password';
        return redirect '/login';
    }
    else{
        my $user= $account_user->get_column('account_owner');
        $username = $account_user->get_column('account_user');
        if ($params->{'username'} ne $username || $params->{'password'} ne $account_password->get_column('account_password')){
            $err = 'Invalid username or password';
            return redirect '/login';
        }
        else {
            my $userkey=$account_user->get_column('account_key');
            if ($userkey){
                if ($params->{'key'} && generateCurrentNumber($userkey) eq $params->{'key'}){
                    my $role= $schema->resultset('Client')->find(
                        {
                            client_id => $user
                        },
                        {
                            fetch => 'admin'
                        }
                    );
                    $client_id = $user ; 
                    if ($role->admin == 1)
                    {
                        $err = undef;
                        session 'loged_in' => true ;
                        return redirect '/homeadmin';
                    }
                    else{
                        $err = undef;
                        session 'loged_in' => true ;
                        return redirect '/homeuser';
                    }
                }
                else{
                    $err = 'Insert your authenticator key';
                    return redirect '/login';
                }
            }
            else{
                my $role= $schema->resultset('Client')->find(
                    {
                        client_id => $user
                    },
                    {
                        fetch => 'admin'
                    }
                );
                $client_id = $user ; 
                if ($role->admin == 1)
                {
                    $err = undef;
                    session 'loged_in' => true ;
                    return redirect '/homeadmin';
                }
                else{
                    $err = undef;
                    session 'loged_in' => true ;
                    return redirect '/homeuser';
                }                
            }
        }
    }
};

post '/homeadmindomain' => sub {
    my $params = request->params;
    if (!$params->{'domainName'} || !$params->{'domainOwner'} || !$params->{'domainContact'} || !$params->{'domainList'} || !($params->{'domainName'}=~m/\.[a-z]{2,4}/)){
        $err = 'Please insert all mandatory fields or a corect domain';
        $crtdomain=undef;
        return redirect '/homeadmin';
    }
    else{
            my $datestring = strftime "%Y-%m-%d %H:%M:%S" , localtime;
            my $dateperiod = DateTime->new (
            year   => (strftime"%Y" , localtime) + params->{'domainList'},
            month  => (strftime"%m" , localtime) ,
            day    => (strftime"%d" , localtime) , 
            hour   => (strftime"%H" , localtime) ,
            minute => (strftime"%M" , localtime) ,
            second => (strftime"%S" , localtime)-1 ,
        );
        my $newdomain = $schema->resultset('Domain')->new(
            {
                domain_name         => $params->{'domainName'},
                domain_owner        => $params->{'domainOwner'},
                domain_tech_contact => $params->{'domainContact'},
                domain_aq_date      => $datestring,
                domain_period       => $dateperiod->strftime( '%Y-%m-%d %H:%M:%S' ),
                domain_nameserver   => $params->{'domainNameserver'},
            }
        );
        $newdomain->insert();
        $crtdomain = 'Domain succesfully created.';
        return redirect '/clear';
    }
};

post '/homeuserdomain' => sub {
    my $params = request->params;
    if (!$params->{'domainName'} || !$params->{'domainOwner'} || !$params->{'domainContact'} || !$params->{'domainList'} || !($params->{'domainName'}=~m/\.[a-z]{2,4}/)){
        $err = 'Please insert all mandatory fields or a corect domain';
        $crtdomain=undef;
        return redirect '/homeuser';
    }
    else{
            my $datestring = strftime "%Y-%m-%d %H:%M:%S" , localtime;
            my $dateperiod = DateTime->new (
            year   => (strftime"%Y" , localtime) + params->{'domainList'},
            month  => (strftime"%m" , localtime) ,
            day    => (strftime"%d" , localtime) , 
            hour   => (strftime"%H" , localtime) ,
            minute => (strftime"%M" , localtime) ,
            second => (strftime"%S" , localtime)-1 ,
        );
        my $newdomain = $schema->resultset('Domain')->new(
            {
                domain_name         => $params->{'domainName'},
                domain_owner        => $params->{'domainOwner'},
                domain_tech_contact => $params->{'domainContact'},
                domain_aq_date      => $datestring,
                domain_period       => $dateperiod->strftime( '%Y-%m-%d %H:%M:%S' ),
                domain_nameserver   => $params->{'domainNameserver'},
            }
        );
        $newdomain->insert();
        $crtdomain = 'Domain succesfully created.';
        return redirect '/clearuser';
    }
};

post '/homeadminaccount' => sub {
    my $params = request->params;
    if (!$params->{'accountPassword'} || !$params->{'accountName'} || !$params->{'accountId'}){
        $erraccount = 'Please insert valid information';
        $crtaccount = undef ;
        return redirect '/homeadmin';
    }
    else {
        my $newaccount = $schema->resultset('Account')->new(
            {
                account_user => $params->{'accountName'},
                account_password => $params->{'accountPassword'},
                account_owner => $params->{'accountId'}
            }
        );
        $newaccount -> insert();
        $crtaccount = 'Account succesfully created.';
        return redirect '/clear';
    }
};

post '/homeadminuser' => sub {
    my $params = request->params;
    if (!$params->{'clientName'}||!$params->{'clientSurname'}||!$params->{'clientAdress'}||!$params->{'clientCountry'}||!$params->{'clientCity'}){
        $erruser = 'Please complete the mandatory fields marked with * and add a phone or email';
        $crtuser = undef;
        return redirect '/homeadmin';
    }
        if (!$params->{'clientEmail'}  && !$params->{'clientPhone'}){
            $erruser = 'Please add a phone number or email adress';
            $crtuser = undef;
            return redirect '/homeadmin';
        }
        else{
            my $newuser = $schema->resultset('Client')->new(
                {
                    client_name    => $params->{'clientName'},
                    client_surname => $params->{'clientSurname'},
                    client_adress  => $params->{'clientAdress'},
                    client_city    => $params->{'clientCity'},
                    client_country => $params->{'clientCountry'},
                    client_phone   => $params->{'clientPhone'},
                    client_email   => $params->{'clientEmail'},
                    admin          => $params->{'clientAdmin'}
                }
            );
            $newuser -> insert();
            $crtuser= 'User created succesfully.';
            return redirect '/clear';
        }
};

get '/logout' => sub {
    session 'loged_in' => false;
    $err = undef ;
    return redirect '/login';
};

get '/clear' => sub {
    $err = undef;
    $erraccount = undef;
    $erruser = undef;
    $errregister = undef;
    return redirect '/homeadmin';
};

get '/clearuser' => sub {
    $err = undef;
    $erraccount = undef;
    $erruser = undef;
    $errregister = undef;
    return redirect '/homeuser';
};

post '/edit_profile' => sub {
    my $params = request->params;
    my $user = $schema->resultset('Client')->find(
        {
            client_id => $client_id
        },
    );
    if (!$params->{'newName'}||!$params->{'newSurname'}||!$params->{'newAdress'}||!$params->{'newCountry'}||!$params->{'newCity'}){
        $errregister = 'Please complete the mandatory fields marked with * and add a phone or email';
        print Dumper($errregister);
        return redirect '/edit_profile';
    }
    else{
        if (!$params->{'newEmail'}  && !$params->{'newPhone'}){
            $errregister = 'Please add a phone number or email adress';
            print Dumper($errregister);
            return redirect '/edit_profile';
        }
        else{
            $user->update(
                {
                    client_name    => $params->{'newName'},
                    client_surname => $params->{'newSurname'},
                    client_adress  => $params->{'newAdress'},
                    client_city    => $params->{'newCity'},
                    client_country => $params->{'newCountry'},
                    client_email   => $params->{'newEmail'},
                    client_phone   => $params->{'newPhone'},
                }
            );
            return redirect '/clear';  
        }
    }  
};

post '/edit_profileuser' => sub {
    my $params = request->params;
    my $user = $schema->resultset('Client')->find (
        {
            client_id => $client_id
        },
    );
    if (!$params->{'newName'}||!$params->{'newSurname'}||!$params->{'newAdress'}||!$params->{'newCountry'}||!$params->{'newCity'}){
        $errregister = 'Please complete the mandatory fields marked with * and add a phone or email';
        print Dumper($errregister);
        return redirect '/edit_profileuser';
    }
    else{
        if (!$params->{'newEmail'}  && !$params->{'newPhone'}){
            $errregister = 'Please add a phone number or email adress';
            print Dumper($errregister);
            return redirect '/edit_profileuser';
        }
        else{
            $user->update(
                {
                    client_name    => $params->{'newName'},
                    client_surname => $params->{'newSurname'},
                    client_adress  => $params->{'newAdress'},
                    client_city    => $params->{'newCity'},
                    client_country => $params->{'newCountry'},
                    client_email   => $params->{'newEmail'},
                    client_phone   => $params->{'newPhone'},
                }
            );
            return redirect '/clearuser';  
        }
    }  
};

get '/edit_profile' => sub {
    my $user = $schema->resultset('Client')->find(
        {
            client_id => $client_id
        }
    );
    my $username    = $user->get_column('client_name');
    my $usersurname = $user->get_column('client_surname');
    my $useradress  = $user->get_column('client_adress');
    my $usercity    = $user->get_column('client_city');
    my $usercountry = $user->get_column('client_country');
    my $userphone   = $user->get_column('client_phone');
    my $useremail   = $user->get_column('client_email');
    template 'Profile.tt' , {
        'oldname'    => $username,
        'oldsurname' => $usersurname,
        'oldadress'  => $useradress,
        'oldcity'    => $usercity,
        'oldcountry' => $usercountry,
        'oldemail'   => $useremail,
        'oldphone'   => $userphone,
        'Back'       => '/homeadmin',
        'Register'   => '/edit_profile',
        'err'        => $errregister, 
    };
};

get '/edit_profileuser' => sub {
    my $user = $schema->resultset('Client')->find(
        {
            client_id => $client_id
        }
    );
    my $username    = $user->get_column('client_name');
    my $usersurname = $user->get_column('client_surname');
    my $useradress  = $user->get_column('client_adress');
    my $usercity    = $user->get_column('client_city');
    my $usercountry = $user->get_column('client_country');
    my $userphone   = $user->get_column('client_phone');
    my $useremail   = $user->get_column('client_email');
    template 'Profile.tt' , {
        'oldname'    => $username,
        'oldsurname' => $usersurname,
        'oldadress'  => $useradress,
        'oldcity'    => $usercity,
        'oldcountry' => $usercountry,
        'oldemail'   => $useremail,
        'oldphone'   => $userphone,
        'Back'       => '/homeuser',
        'Register'   => '/edit_profileuser',
        'err'        => $errregister, 
    };
};

get '/homeadmin' => sub {
    my $user = $schema->resultset('Client')->find(
        {
            client_id => $client_id,
        },
    );
    my $username    = $user->get_column('client_name');
    my $usersurname = $user->get_column('client_surname');
    my $useradress  = $user->get_column('client_adress');
    my $usercity    = $user->get_column('client_city');
    my $usercountry = $user->get_column('client_country');
    my $userphone   = $user->get_column('client_phone');
    my $useremail   = $user->get_column('client_email');
    template 'index.tt' , {
        'username'    => $username,
        'usersurname' => $usersurname,
        'userid'      => $client_id,
        'useradress'  => $useradress,
        'usercity'    => $usercity,
        'usercountry' => $usercountry,
        'userphone'   => $userphone,
        'useremail'   => $useremail,
        'err'         => $err,
        'add_domain'  => '/homeadmindomain',
        'crtdomain'   => $crtdomain,
        'crtuser'     => $crtuser,
        'crtaccount'  => $crtaccount,
        'erruser'     => $erruser,
        'add_user'    => '/homeadminuser',
        'erraccount'  => $erraccount,
        'add_account' => '/homeadminaccount',
        'logout'      => '/logout',
        'edit_profile'=> '/edit_profile',
        'mydomains'   => '/mydomains',
        'view_domain' => '/domains'
    };

};

post '/domains' => sub {
    my $params = request->params;
    $term = $params->{'term'};
    return redirect '/domains';
};

get '/domains' => sub {
    print Dumper($term);
    template 'alldomains.tt' , {
        'search_domain'  => '/domains',
        'back'           => '/back',
        'stuff'           => $term,
    };
};

get '/homeuser' => sub {
    my $user = $schema->resultset('Client')->find(
        {
            client_id => $client_id,
        },
    );
    my $username    = $user->get_column('client_name');
    my $usersurname = $user->get_column('client_surname');
    my $useradress  = $user->get_column('client_adress');
    my $usercity    = $user->get_column('client_city');
    my $usercountry = $user->get_column('client_country');
    my $userphone   = $user->get_column('client_phone');
    my $useremail   = $user->get_column('client_email');
    template 'userindex.tt' , {
        'username'    => $username,
        'usersurname' => $usersurname,
        'userid'      => $client_id,
        'useradress'  => $useradress,
        'usercity'    => $usercity,
        'usercountry' => $usercountry,
        'userphone'   => $userphone,
        'useremail'   => $useremail,
        'err'         => $err,
        'add_domain'  => '/homeuserdomain',
        'crtdomain'   => $crtdomain,
        'logout'      => '/logout',
        'edit_profile'=> '/edit_profileuser',
        'mydomains'   => '/mydomains',
        'view_domain'=> '/domains',
    };
};

post '/back' => sub {
    $term = undef ;
    my $user = $schema->resultset('Client')->find(
        {
            client_id => $client_id,
        },
    );
    my $status = $user->get_column('admin');
    if ($status == 1){
        return redirect '/homeadmin';
    }
    else{
        return redirect '/homeuser';
    }
};

get '/mydomains' => sub {
    template 'mydomains.tt' , {
        'client_id'   => $client_id,
        'back'        => '/back',
        
    };
};

get '/' => sub {
    
     redirect '/login';
};

sub generateBase32Secret {
    my @chars = ("A".."Z", "2".."7");
    my $length = scalar(@chars);
    my $base32Secret = "";
    for (my $i = 0; $i < 16; $i++) {
	$base32Secret .= $chars[rand($length)];
    }
    return $base32Secret;
}

sub generateCurrentNumber {
    my ($base32Secret) = @_;
    my $paddedTime = sprintf("%016x", int(time() / $TIME_STEP));
    my $data = pack('H*', $paddedTime);
    my $key = decodeBase32($base32Secret);
    my $hmac = hmac_sha1_hex($data, $key);
    my $offset = hex(substr($hmac, -1));
    my $encrypted = hex(substr($hmac, $offset * 2, 8)) & 0x7fffffff;
    my $token = $encrypted % 1000000;
    return sprintf("%06d", $token);
}

sub decodeBase32 {
    my ($val) = @_;
    $val =~ tr|A-Z2-7|\0-\37|;
    $val = unpack('B*', $val);
    $val =~ s/000(.....)/$1/g;
    my $len = length($val);
    $val = substr($val, 0, $len & ~7) if $len & 7;
    $val = pack('B*', $val);
    return $val;
}

true;
